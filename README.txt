
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module adds an image-style which crops an image based on the configured
width or height againest the image style.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core. Core image module must be enabled. 

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------



MAINTAINERS
-----------------

Current maintainers:
 * Karteek Reddy Talusani - https://www.drupal.org/u/karteektalusanipennywisesolutions
 
This project has been sponsored by:
 * PennyWise Solutions Private Limited
   Specialized in consulting and planning of Drupal powered sites,
   PennyWise Solutions Private Limited offers installation, development,
   theming,customization, and hosting to get you started.
   Visit https://www.pennywisesolutions.com for more information.
