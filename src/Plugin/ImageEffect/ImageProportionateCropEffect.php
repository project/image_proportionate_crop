<?php

namespace Drupal\image_proportional_crop\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\ImageEffect\CropImageEffect;

/**
 * Crop an image resource with respect for max size,and with only one Dimension.
 *
 * @ImageEffect(
 *   id = "image_proportional_crop",
 *   label = @Translation("Proportional Image Crop"),
 *   description = @Translation("Cropping the images Proportional based on the given width/height.")
 * )
 */
class ImageProportionateCropEffect extends CropImageEffect {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    /*Set the desired dimensions - if they don't change do nothing*/
    $desired_width = $image->getWidth();
    $desired_height = $image->getHeight();
    if ($this->configuration['width'] && $this->configuration['width'] < $image->getWidth()) {
      $desired_width = $this->configuration['width'];
    }
    if ($this->configuration['height'] && $this->configuration['height'] < $image->getHeight()) {
      $desired_height = $this->configuration['height'];
    }
    if ($image->getWidth() == $desired_width && $image->getHeight() == $desired_height) {
      return TRUE;
    }
    $newHeight = $desired_width * ($image->getHeight() / $image->getWidth());
    /*list($x, $y) = explode('-', $this->configuration['anchor']);*/
    if (!$image->scaleAndCrop($desired_width, $newHeight)) {
      $this->logger->error('Image crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)',
      [
        '%toolkit' => $image->getToolkitId(),
        '%path' => $image->getSource(),
        '%mimetype' => $image->getMimeType(),
        '%dimensions' => $image->getWidth() . 'x' . $image->getHeight(),
      ]
      );
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    unset($form['width']['#required']);
    unset($form['height']['#required']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->isValueEmpty('width') && $form_state->isValueEmpty('height')) {
      $form_state->setErrorByName('data', $this->t('Width and height can not both be blank.'));
    }
  }

}
